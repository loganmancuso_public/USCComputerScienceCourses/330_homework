% solution(France,Switzerland,Italy,Belgium,Holland) holds if France,Switzerland,Italy,Belgium,Holland are colors 
% that solve a map-coloring problem from the text.
solution(France,Switzerland,Italy,Belgium,Holland,Germany,Austria) :-
   color(France), color(Switzerland), color(Italy), color(Belgium), color(Holland), color(Germany), color(Austria), 
   \+ France=Switzerland, \+ France=Italy, \+ France=Belgium, \+ France=Germany, \+ Switzerland=Italy, 
   \+ Italy=Austria, \+ Belgium=Germany, \+ Belgium=Holland, \+ Holland=Germany, \+ Germany=Austria.

% The three colors are these.
color(red).
color(yellow).
color(orange).
