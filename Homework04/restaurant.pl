% A second logic puzzle involving people and meals and drinks
beside(X,Y) :- position(X), Y is (X+1) mod 2.
across(X,Y) :- position(X), Y is (X+2) mod 4.
position(1).  
position(2).  
position(3). 
position(4).

solution(Donna,Danny,David,Doreen,
   Pizza,Lasagna,Chicken,Steak,
   Milk,Water,Coke,Coffee) :-

   across(Milk,Lasagna),
   beside(Steak,Doreen),
   across(David,Danny),
   across(Doreen,Donna),
   Chicken = Coke,     
   uniq_position(Donna,Danny,David,Doreen),
   uniq_position(Chicken,Steak,Pizza,Lasagna),
   uniq_position(Milk,Water,Coffee,Coke),
   \+ David = Coffee, 
   Donna = Water,     
   \+ Danny = Steak,   
   Donna = Water.


uniq_position(A,B,C,D) :- 
   position(A),  position(B),  position(C), position(D), 
   \+ A=B, \+ A=C, \+ A=D, \+ B=C, \+ B=D, \+ C=D.


