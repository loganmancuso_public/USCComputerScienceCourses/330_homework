%/****************************************************************
 %* 'homework02.pl'
 %*
 %* author/copyright: mancuso, logan
 %* last edit date: 09-07-2017--15:21:29
 %*
%**/

%/****************************************************************
% * atomic sentences
%**/

%director
directed( alejandro,babel ).
directed( neill_blomkamp, district_9 ).
directed( frank_coraci, click ).
directed( martin_scorsese, the_aviator ).
directed( rawson_marshall_thurber, were_the_millers ).
directed( seth_gordon, horrible_bosses ).
directed( josh_gordon, the_switch ).
directed( will_speck, the_switch ).
directed( brad_bird, ratatouille ).
directed( jan_pinkava, ratatouille ).
directed( brad_bird, the_incredibles ).
directed( lana_wachowski, the_matrix ).
directed( lilly_wachowski, the_matrix ).
directed( christopher_nolan, inception ).
directed( martin_scorsese, shutter_island ).


% acted_in( person,movie )  % the person acted in the movie
acted_in( cate_blanchett, babel ).
acted_in( brad_pitt, babel ).
acted_in( nathalie_boltt, district_9 ).
acted_in( sharlto_copley, district_9 ).
acted_in( jason_cope, district_9 ).
acted_in( adam_sandler, click ).
acted_in( kate_beckinsale, click ).
acted_in( christopher_walken, click ).
acted_in( david_hasslhoff, click ).
acted_in( henry_winkler, click ).
acted_in( leonardo_dicaprio, the_aviator ).
acted_in( cate_blanchett, the_aviator ).
acted_in( kate_beckinsale, the_aviator ).
acted_in( jennifer_aniston, were_the_millers ).
acted_in( jason_sudeikis, were_the_millers ).
acted_in( emma_roberts, were_the_millers ).
acted_in( jennifer_aniston, horrible_bosses ).
acted_in( jason_bateman, horrible_bosses ).
acted_in( kevin_spacey, horrible_bosses ).
acted_in( jennifer_aniston, the_switch ).
acted_in( jason_bateman, the_switch ).
acted_in( jeff_goldblum, the_switch ).
acted_in( patton_oswalt, ratatouille ).
acted_in( ian_holm, ratatouille ).
acted_in( lou_romano, ratatouille ).
acted_in( samuel_jackson, the_incredibles ).
acted_in( holly_hunter, the_incredibles ).
acted_in( keanu_reeves, the_matrix ).
acted_in( laurence_fishburne, the_matrix ).
acted_in( hugo_weaving, the_matrix ).
acted_in( leonardo_dicaprio, inception ).
acted_in( ellen_page, inception ).
acted_in( joseph_gordon_levitt, inception ).
acted_in( tom_hardy, inception ).
acted_in( leonardo_dicaprio, shutter_island ).
acted_in( mark_ruffalo, shutter_island ).
acted_in( ben_kingsley, shutter_island ).

% directed( person,movie )  % the person directed the movie
directed( alejandro, babel ).
directed( neill_blomkamp, district_9 ).
directed( frank_coraci, click ).
directed( martin_scorsese, the_aviator ).
directed( rawson_marshall_thurber, were_the_millers ).
directed( seth_gordon, horrible_bosses ).
directed( josh_gordon, the_switch ).
directed( will_speck, the_switch ).
directed( brad_bird, ratatouille ).
directed( jan_pinkava, ratatouille ).
directed( brad_bird, the_incredibles ).
directed( lana_wachowski, the_matrix ).
directed( lilly_wachowski, the_matrix ).
directed( christopher_nolan, inception ).
directed( martin_scorsese, shutter_island ).

% released( movie,year )  % the movie came out that year
released( babel, 2006 ).
released( district_9, 2009 ).
released( click, 2006 ).
released( the_aviator, 2004 ).
released( were_the_millers, 2013 ).
released( horrible_bosses, 2011 ).
released( the_switch, 2010 ).
released( ratatouille, 2007 ).
released( the_incredibles, 2004 ).
released( the_matrix, 1999 ).
released( inception, 2010 ).
released( shutter_island, 2010 ).

%/****************************************************************
% * conditional statements
%**/

% same actor Z in both movies Y and Z
in_both( X, Y ) :-
  acted_in( X, Y ),
  acted_in( X, Z ).
% find movie Y released in some date X
released_in( X, Y ) :-
  released( X, Y ).
% same director X different movie Y, Z
more_than_one_movie( X, Y, Z ) :-
  directed( X, Y ),
  directed( X, Z ).
% different directors X,Y same movie Z
more_than_one_director( X, Y, Z ) :-
  directed( X, Z ),
  directed( Y, Z ).
% actor X in more than one movie in the same year
more_than_one_movie_year( X, Y, Z ) :-
  acted_in( X, Y ),
  acted_in( X, Z ),
  released_in( X, Z ),
  released_in( Y, Z ).

%/****************************************************************
% * end 'homework02.pl'
%**/
